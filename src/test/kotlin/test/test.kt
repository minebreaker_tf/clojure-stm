package test

import clojure.lang.LockingTransaction
import clojure.lang.Ref
import com.google.common.truth.Truth.assertThat
import org.junit.jupiter.api.Test
import rip.deadcode.izvestia.Core.expect
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class Tester {

    @Test
    fun testCommit() {

        val param = Ref(100)

        LockingTransaction.runInTransaction {
            param.set(0)
            println(param.deref())
        }

        assertThat(param.deref()).isEqualTo(0)
    }

    @Test
    fun testRevert() {

        val param = Ref(100)

        try {
            LockingTransaction.runInTransaction {
                param.set(0)
                throw RuntimeException()
            }
        } catch (e: RuntimeException) {
        }

        assertThat(param.deref()).isEqualTo(100)
    }

    @Test
    fun testConcurrency() {

        val p1 = Ref(5050)
        val p2 = Ref(0)
        val executor = Executors.newFixedThreadPool(16)

        executor.invokeAll((1..100).map { i ->
            Callable {
                LockingTransaction.runInTransaction {
                    p1.set(p1.deref() as Int - i)
                    p2.set(p2.deref() as Int + i)
                }
            }
        })

        executor.shutdown()
        executor.awaitTermination(10, TimeUnit.SECONDS)

        assertThat(p1.deref()).isEqualTo(0)
        assertThat(p2.deref()).isEqualTo(5050)
    }

    @Test
    fun testNotInTransaction() {

        expect {
            val r = Ref(0)
            r.set(100)

        }.throwsException {
            assertThat(it).isInstanceOf(IllegalStateException::class.java)
            assertThat(it).hasMessageThat().isEqualTo("No transaction running")
        }
    }

    @Test
    fun test() {

        val ref = Ref(0)

        val executor = Executors.newSingleThreadExecutor()

        LockingTransaction.runInTransaction {
            println("A: " + ref.deref())
            ref.set(100)

            executor.submit {
                LockingTransaction.runInTransaction {
                    println("B: " + ref.deref())
                    ref.set(200)
                    println("C: " + ref.deref())
                }
            }

            println("D: " + ref.deref())
        }

        executor.shutdown()
        executor.awaitTermination(10, TimeUnit.SECONDS)
    }
}
