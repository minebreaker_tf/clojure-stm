/**
 * Copyright (c) Rich Hickey. All rights reserved.
 * The use and distribution terms for this software are covered by the
 * Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 * which can be found in the file epl-v10.html at the root of this distribution.
 * By using this software in any fashion, you are agreeing to be bound by
 * the terms of this license.
 * You must not remove this notice, or any other, from this software.
 **/

/* rich Mar 25, 2006 4:28:27 PM */

package clojure.lang;

import wrapper.PseudoPersistentList;

import static org.organicdesign.fp.StaticImports.vec;

public class RT {

    public static ISeq cons(Object x, Object coll) {
        if (coll == null)
            return new PseudoPersistentList(vec(x));
        else if (coll instanceof ISeq)
            return new Cons(x, (ISeq) coll);
        else
            return new Cons(x, new PseudoPersistentList(vec(x)));
    }

    public static boolean booleanCast(Object x) {
        if (x instanceof Boolean)
            return (Boolean) x;
        return x != null;
    }

}
