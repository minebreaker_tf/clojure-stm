package wrapper;

import clojure.lang.ISeq;
import org.organicdesign.fp.collections.ImList;

import static org.organicdesign.fp.StaticImports.vec;

public final class PseudoPersistentList implements ISeq {

    private ImList<Object> in;

    public PseudoPersistentList() {
        this.in = vec();
    }

    public PseudoPersistentList(ImList<Object> in) {
        this.in = in;
    }

    @Override
    public Object first() {
        return in.get(0);
    }

    @Override
    public ISeq next() {
        return new PseudoPersistentList(in.drop(1).toImList());
    }

    @Override
    public ISeq more() {
        return null;
    }

    @Override
    public int count() {
        return in.size();
    }

    @Override
    public ISeq seq() {
        return null;
    }
}
