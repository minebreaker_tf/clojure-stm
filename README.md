# Clojure STM

The ongoing attempt to port Clojure's software transactional memory to Java.


## License

The most part copied from Clojure is available under the EPL 1.0, same as Clojure.
Other part I've written is MIT licensed. 
