package wrapper;

import clojure.lang.IPersistentMap;
import clojure.lang.ISeq;
import org.organicdesign.fp.collections.ImMap;

import static org.organicdesign.fp.StaticImports.map;

public final class PseudoPersistentMap implements IPersistentMap {

    private ImMap<Object, Object> in;

    public PseudoPersistentMap() {
        this.in = map();
    }

    public PseudoPersistentMap(ImMap<Object, Object> in) {
        this.in = in;
    }

    @Override
    public IPersistentMap assoc(Object key, Object val) {
        return new PseudoPersistentMap(in.assoc(key, val));
    }

    @Override
    public IPersistentMap without(Object key) {
        return new PseudoPersistentMap(in.without(key));
    }

    @Override
    public int count() {
        return in.size();
    }

    @Override
    public ISeq seq() {
        return new PseudoPersistentList(in.entrySet().toImList().map(e -> (Object) e).toImList());
    }
}
